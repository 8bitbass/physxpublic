#pragma once
#include <PxPhysicsAPI.h>
class RagdollNode;
using namespace physx;

class RagDoll {
public:
    RagDoll();
    ~RagDoll();

    enum RagDollParts {
        NO_PARENT = -1,
        LOWER_SPINE,
        LEFT_PELVIS,
        RIGHT_PELVIS,
        LEFT_UPPER_LEG,
        RIGHT_UPPER_LEG,
        LEFT_LOWER_LEG,
        RIGHT_LOWER_LEG,
        LEFT_FOOT,
        RIGHT_FOOT,
        UPPER_SPINE,
        LEFT_CLAVICLE,
        RIGHT_CLAVICLE,
        NECK,
        HEAD,
        LEFT_UPPER_ARM,
        RIGHT_UPPER_ARM,
        LEFT_LOWER_ARM,
        RIGHT_LOWER_ARM,
        LEFT_HAND,
        RIGHT_HAND,
    };

    static PxArticulation* MakeRagDoll(PxPhysics* g_Physics, RagdollNode** nodeArray, PxTransform worldPos, float scaleFactor, PxMaterial* ragdollMaterial);

    //create some constants for axis of rotation to make definition of quaternions a bit neater
    const PxVec3 X_AXIS = PxVec3(1, 0, 0);
    const PxVec3 Y_AXIS = PxVec3(0, 1, 0);
    const PxVec3 Z_AXIS = PxVec3(0, 0, 1);

};

class RagdollNode {
public:
    //rotation of this link in model space - we could have done this relative to the parent node but it's harder to visualize when setting up the data by hand
    PxQuat globalRotation;
    //Position of the link centre in world space which is calculated when we process the node. It's easiest if we store it here so we have it when we transform the child
    PxVec3 scaledGobalPos;
    //Index of the parent node
    int parentNodeIdx;
    //half length of the capsule for this node
    float halfLength;
    //radius of capsule for this node
    float radius;
    //relative position of link centre in parent to this node. 0 is the centre of the node, -1 is left end of capsule and 1 is right end of capsule relative to x
    float parentLinkPos;
    //relative position of link centre in child
    float childLinkPos;
    //name of link
    char* name;
    PxArticulationLink* linkPtr;

    //constructor
    RagdollNode(PxQuat _globalRotation, int _parentNodeIdx, float _halfLength, float _radius, float _parentLinkPos, float _childLinkPos, char* _name) {
        globalRotation = _globalRotation, parentNodeIdx = _parentNodeIdx;
        halfLength = _halfLength;
        radius = _radius;
        parentLinkPos = _parentLinkPos;
        childLinkPos = _childLinkPos;
        name = _name;
    };
};