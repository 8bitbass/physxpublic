#include "MyControllerHitReport.h"

void MyControllerHitReport::onShapeHit(const physx::PxControllerShapeHit& hit) {
    //gets a reference to a structure which tells us what has been hit and where 
    //get the acter from the shape we hit
    physx::PxRigidActor* actor = hit.actor;
    //get the normal of the thing we hit and store it so the player controller can respond correctly
    _playerContactNormal = hit.worldNormal;
    //try to cast to a dynamic actor
    physx::PxRigidDynamic* myActor = reinterpret_cast<PxRigidDynamic*>(actor);;
    if (myActor) {
        //this is where we can apply forces to things we hit
    }
}

