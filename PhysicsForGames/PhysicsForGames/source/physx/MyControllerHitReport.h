#pragma once
#include <characterkinematic/PxController.h>

using namespace physx;

class MyControllerHitReport : public physx::PxUserControllerHitReport {
public:
    //overload the onShapeHit function
    virtual void onShapeHit(const physx::PxControllerShapeHit& hit);

    //other collision functions which we must overload 
    //these handle collision with other controllers and hitting obstacles
    virtual void onControllerHit(const physx::PxControllersHit& hit) {};

    //Called when current controller hits another controller. More...
    virtual void onObstacleHit(const physx::PxControllerObstacleHit& hit) {};

    //Called when current controller hits a user-defined obstacl
    MyControllerHitReport(): PxUserControllerHitReport() {};

    physx::PxVec3 getPlayerContactNormal() {
        return _playerContactNormal;
    };

    void clearPlayerContactNormal() {
        _playerContactNormal = physx::PxVec3(0, 0, 0);
    };

    physx::PxVec3 _playerContactNormal;
};

