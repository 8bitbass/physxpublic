#pragma once
#include <foundation/Px.h>
#include <PxSimulationEventCallback.h>

using namespace physx;
class MycollisionCallBack : public PxSimulationEventCallback {
public:
    virtual void onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs);

    virtual void onTrigger(PxTriggerPair* pairs, PxU32 nbPairs);

    virtual void onConstraintBreak(PxConstraintInfo*, PxU32) {};

    virtual void onWake(PxActor**, PxU32) {};

    virtual void onSleep(PxActor**, physx::PxU32) {};
};

