#include "PhysxPhysics.h"
#include "../../ParticleEmitter.h"
#include "../../ParticleFluidEmitter.h"
#include "MycollisionCallBack.h"

class myAllocator : public PxAllocatorCallback {
public:
    virtual ~myAllocator() {}

    virtual void* allocate(size_t size, const char* typeName, const char* filename, int line) {
        void* pointer = _aligned_malloc(size, 16);
        return pointer;
    }

    virtual void deallocate(void* ptr) {
        _aligned_free(ptr);
    }
};

PhysxPhysics::PhysxPhysics() {}


PhysxPhysics::~PhysxPhysics() {}

PxFilterFlags myFliterShader(PxFilterObjectAttributes attributes0, PxFilterData filterData0, PxFilterObjectAttributes attributes1, PxFilterData filterData1, PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize) {
    // let triggers through
    if (PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1)) {
        pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
        return PxFilterFlag::eDEFAULT;
    }
    // generate contacts for all that were not filtered above
    pairFlags = PxPairFlag::eCONTACT_DEFAULT;
    // trigger the contact callback for pairs (A,B) where
    // the filtermask of A contains the ID of B and vice versa.
    if ((filterData0.word0 & filterData1.word1) && (filterData1.word0 & filterData0.word1))
        pairFlags |= PxPairFlag::eNOTIFY_TOUCH_FOUND | PxPairFlag::eNOTIFY_TOUCH_LOST;
    return PxFilterFlag::eDEFAULT;
}

void PhysxPhysics::SetupPhysx() {
    PxAllocatorCallback* myCallback = new myAllocator();
    g_PhysicsFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, *myCallback, gDefaultErrorCallback);
    g_Physics = PxCreatePhysics(PX_PHYSICS_VERSION, *g_PhysicsFoundation, PxTolerancesScale());
    PxInitExtensions(*g_Physics);
    //create physics material 
    g_PhysicsMaterial = g_Physics->createMaterial(0.5f, 0.5f, .5f);
    PxSceneDesc sceneDesc(g_Physics->getTolerancesScale());
    sceneDesc.gravity = PxVec3(0, -10.0f, 0);
    sceneDesc.filterShader = myFliterShader;//&physx::PxDefaultSimulationFilterShader;
    sceneDesc.cpuDispatcher = PxDefaultCpuDispatcherCreate(1);
    g_PhysicsScene = g_Physics->createScene(sceneDesc);

    PxSimulationEventCallback* mycollisionCallBack = new MycollisionCallBack();
    g_PhysicsScene->setSimulationEventCallback(mycollisionCallBack);

    //create our particle system
    PxParticleFluid* pf;
    // create particle system in PhysX SDK // set immutable properties. 
    PxU32 maxParticles = 4000;
    bool perParticleRestOffset = false;
    pf = g_Physics->createParticleFluid(maxParticles, perParticleRestOffset);
    pf->setRestParticleDistance(.3f);
    pf->setDynamicFriction(0.1);
    pf->setStaticFriction(0.1);
    pf->setDamping(0.1);
    pf->setParticleMass(.1);
    pf->setRestitution(0);
    //pf->setParticleReadDataFlag(PxParticleReadDataFlag::eDENSITY_BUFFER, // true);
    pf->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
    pf->setStiffness(100);
    if (pf) {
        g_PhysicsScene->addActor(*pf);
        m_particleEmitter = new ParticleFluidEmitter(maxParticles, PxVec3(0, 10, 0), pf, .1);
        m_particleEmitter->setStartVelocityRange(-0.001f, -250.0f, -0.001f, 0.001f, -250.0f, 0.001f);
    }

}

void PhysxPhysics::UpdatePhysx(float deltaTime) const {
    if (deltaTime <= 0) {
        return;
    }
    g_PhysicsScene->simulate(deltaTime);
    while (g_PhysicsScene->fetchResults() == false) {
        // don�t need to do anything here yet but we have to fetch results 
    }
    if (m_particleEmitter) {
        m_particleEmitter->update(deltaTime); //render all our particles
        m_particleEmitter->renderParticles();
    }
}

void PhysxPhysics::SetupVisualDebugger() {
    // check if PvdConnection manager is available on this platform
    if (g_Physics->getPvdConnectionManager() == NULL) {
        return;
    }
    // setup connection parameters
    const char* pvd_host_ip = "127.0.0.1";
    // IP of the PC which is running PVD
    int port = 5425;
    // TCP port to connect to, where PVD is listening
    unsigned int timeout = 100;
    // timeout in milliseconds to wait for PVD to respond,
    //consoles and remote PCs need a higher timeout.
    PxVisualDebuggerConnectionFlags connectionFlags = PxVisualDebuggerExt::getAllConnectionFlags();
    // and now try to connectPxVisualDebuggerExt
    auto theConnection = PxVisualDebuggerExt::createConnection(
        g_Physics->getPvdConnectionManager(),
        pvd_host_ip,
        port,
        timeout,
        connectionFlags
    );
}

void PhysxPhysics::SetupObjects() {
    //add a plane
    PxTransform pose = PxTransform(PxVec3(0.0f, 0, 0.0f), PxQuat(PxHalfPi * 1.0f, PxVec3(0.0f, 0.0f, 1.0f)));
    PxRigidStatic* plane = PxCreateStatic(*g_Physics, pose, PxPlaneGeometry(), *g_PhysicsMaterial);
    //add it to the physX scene
    g_PhysicsScene->addActor(*plane);

    //create a box
    float density = 10;
    PxBoxGeometry box(2, 2, 2);
    PxTransform transform(PxVec3(4, 2, 0));
    PxRigidStatic* staticActor = PxCreateStatic(*g_Physics, transform, box, *g_PhysicsMaterial);
    //add it to the physX scene
    setupFiltering(staticActor, FilterGroup::eGROUND, FilterGroup::ePLAYER);
    staticActor->setName("box");
    setShapeAsTrigger(staticActor);
    g_PhysicsScene->addActor(*staticActor);

    //add a capsule
    PxCapsuleGeometry capsule(2, 2);
    PxTransform capsuleTransform(PxVec3(0, 40, 0));
    PxRigidDynamic* dynamicCapsuleActor = PxCreateDynamic(*g_Physics, capsuleTransform, capsule, *g_PhysicsMaterial, density);
    g_PhysicsScene->addActor(*dynamicCapsuleActor);

    //add a sphere
    PxSphereGeometry ball(2);
    PxTransform ballTransform(PxVec3(0, 20, 0));
    PxRigidDynamic* dynamicBallActor = PxCreateDynamic(*g_Physics, ballTransform, ball, *g_PhysicsMaterial, density);
    g_PhysicsScene->addActor(*dynamicBallActor);

}

void PhysxPhysics::SetupFluidObjects() {
    PxTransform pose = PxTransform(PxVec3(0.0f, 0, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
    PxRigidStatic* plane = PxCreateStatic(*g_Physics, pose, PxPlaneGeometry(), *g_PhysicsMaterial);
    const PxU32 numShapes = plane->getNbShapes();
    g_PhysicsScene->addActor(*plane);
    PxBoxGeometry side1(4.5, 1, .5);
    PxBoxGeometry side2(.5, 1, 4.5);
    pose = PxTransform(PxVec3(0.0f, 0.5, 4.0f));
    PxRigidStatic* box = PxCreateStatic(*g_Physics, pose, side1, *g_PhysicsMaterial);
    g_PhysicsScene->addActor(*box);
    //g_PhysXActors.push_back(box);
    pose = PxTransform(PxVec3(0.0f, 0.5, -4.0f));
    box = PxCreateStatic(*g_Physics, pose, side1, *g_PhysicsMaterial);
    g_PhysicsScene->addActor(*box);
    //g_PhysXActors.push_back(box);
    pose = PxTransform(PxVec3(4.0f, 0.5, 0));
    box = PxCreateStatic(*g_Physics, pose, side2, *g_PhysicsMaterial);
    g_PhysicsScene->addActor(*box);
    //g_PhysXActors.push_back(box);
    pose = PxTransform(PxVec3(-4.0f, 0.5, 0));
    box = PxCreateStatic(*g_Physics, pose, side2, *g_PhysicsMaterial);
    g_PhysicsScene->addActor(*box);
    //g_PhysXActors.push_back(box);
}

void PhysxPhysics::ShootBall(FlyCamera camera) {
    glm::vec3 cam_pos = camera.world[3].xyz();
    glm::vec3 box_vel = -camera.world[2].xyz() * 20.f;
    PxTransform box_transform(PxVec3(cam_pos.x, cam_pos.y, cam_pos.z));
    //Geometry
    PxSphereGeometry sphere(0.5f);
    //Density
    float density = 10;
    PxRigidDynamic* new_actor = PxCreateDynamic(*g_Physics, box_transform, sphere, *g_PhysicsMaterial, density);

    glm::vec3 direction(-camera.world[2]);
    physx::PxVec3 velocity = physx::PxVec3(direction.x, direction.y, direction.z) * 30;
    new_actor->setLinearVelocity(velocity, true);
    g_PhysicsScene->addActor(*new_actor);
}

void PhysxPhysics::CleanPhysx() const {
    g_PhysicsScene->release();
    g_Physics->release();
    g_PhysicsFoundation->release();
}

//helper function to set up filtering
void PhysxPhysics::setupFiltering(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask) {
    PxFilterData filterData;
    filterData.word0 = filterGroup; // word0 = own ID
    filterData.word1 = filterMask; // word1 = ID mask to filter pairs that trigger a contact callback
    const PxU32 numShapes = actor->getNbShapes();
    PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*) * numShapes, 16);
    actor->getShapes(shapes, numShapes);
    for (PxU32 i = 0; i < numShapes; i++) {
        PxShape* shape = shapes[i];
        shape->setSimulationFilterData(filterData);
    }
    _aligned_free(shapes);
}

void PhysxPhysics::setShapeAsTrigger(PxRigidStatic* actorIn) {
    PxRigidStatic* staticActor = actorIn;
    const PxU32 numShapes = staticActor->getNbShapes();
    PxShape** shapes = static_cast<PxShape**>(_aligned_malloc(sizeof(PxShape*) * numShapes, 16));
    staticActor->getShapes(shapes, numShapes);
    for (PxU32 i = 0; i < numShapes; i++) {
        shapes[i]->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
        shapes[i]->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
    }
}

