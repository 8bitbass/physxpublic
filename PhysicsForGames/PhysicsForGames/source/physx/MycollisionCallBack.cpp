#include "MycollisionCallBack.h"
#include <iostream>
#include <PxRigidActor.h>


void MycollisionCallBack::onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs) {
    for (PxU32 i = 0; i < nbPairs; i++) {
        const PxContactPair& cp = pairs[i]; //only interested in touches found and lost
        if (cp.events & PxPairFlag::eNOTIFY_TOUCH_FOUND) {
            std::cout << "Collision Detected between: ";
            std::cout << pairHeader.actors[0]->getName();
            std::cout << pairHeader.actors[1]->getName() << std::endl;
        }
    }

}

void MycollisionCallBack::onTrigger(PxTriggerPair* pairs, PxU32 nbPairs) {
    for (PxU32 i = 0; i < nbPairs; i++) {
        PxTriggerPair* pair = pairs + i;
        PxActor* triggerActor = pair->triggerActor;
        PxActor* otherActor = pair->otherActor;
        if (otherActor->getName() != nullptr && triggerActor->getName() != nullptr) {
            std::cout << otherActor->getName();
            std::cout << " Entered Trigger ";
            std::cout << triggerActor->getName() << std::endl;
        }
    }
};

