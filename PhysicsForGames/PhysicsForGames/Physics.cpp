#include "Physics.h"

#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"
#include "Gizmos.h"

#include "glm/ext.hpp"
#include "glm/gtc/quaternion.hpp"
#include "source/physx/RagDoll.h"
#include "source/physx/MyControllerHitReport.h"

#define Assert(val) if (val){}else{ *((char*)0) = 0;}
#define ArrayCount(val) (sizeof(val)/sizeof(val[0]))

bool Physics::startup() {
    if (Application::startup() == false) {
        return false;
    }

    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    Gizmos::create();

    m_camera = FlyCamera(1280.0f / 720.0f, 10.0f);
    m_camera.setLookAt(vec3(10, 10, 10), vec3(0), vec3(0, 1, 0));
    m_camera.sensitivity = 3;

    const PxVec3 X_AXIS = PxVec3(1, 0, 0);
    const PxVec3 Y_AXIS = PxVec3(0, 1, 0);
    const PxVec3 Z_AXIS = PxVec3(0, 0, 1);

    RagdollNode* ragdollData[] = {
        new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), RagDoll::NO_PARENT, 1, 3, 1, 1, "lower spine"),
        new RagdollNode(PxQuat(PxPi, Z_AXIS), RagDoll::LOWER_SPINE, 1, 1, -1, 1, "left pelvis"),
        new RagdollNode(PxQuat(0, Z_AXIS), RagDoll::LOWER_SPINE, 1, 1, -1, 1, "right pelvis"),
        new RagdollNode(PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS), RagDoll::LEFT_PELVIS, 5, 2, -1, 1, "L upper leg"),
        new RagdollNode(PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS), RagDoll::RIGHT_PELVIS, 5, 2, -1, 1, "R upper leg"),
        new RagdollNode(PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS), RagDoll::LEFT_UPPER_LEG, 5, 1.75, -1, 1, "L lower leg"),
        new RagdollNode(PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS), RagDoll::RIGHT_UPPER_LEG, 5, 1.75, -1, 1, "R lower leg"),
        new RagdollNode(PxQuat(PxPi / 2.0f, Y_AXIS), RagDoll::LEFT_LOWER_LEG, 1, 1.5, -1.5, 1, "left foot"),
        new RagdollNode(PxQuat(PxPi / 2.0f, Y_AXIS), RagDoll::RIGHT_LOWER_LEG, 1, 1.5, -1.5, 1, "right foot"),
        new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), RagDoll::LOWER_SPINE, 1, 3, 1, -1, "upper spine"),
        new RagdollNode(PxQuat(PxPi, Z_AXIS), RagDoll::UPPER_SPINE, 1, 1.5, 1, 1, "left clavicle"),
        new RagdollNode(PxQuat(0, Z_AXIS), RagDoll::UPPER_SPINE, 1, 1.5, 1, 1, "right clavicle"),
        new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), RagDoll::UPPER_SPINE, 1, 1, 1, -1, "neck"),
        new RagdollNode(PxQuat(PxPi / 2.0f, Z_AXIS), RagDoll::NECK, 1, 3, 1, -1, "HEAD"),
        new RagdollNode(PxQuat(PxPi - .3, Z_AXIS), RagDoll::LEFT_CLAVICLE, 3, 1.5, -1, 1, "left upper arm"),
        new RagdollNode(PxQuat(0.3, Z_AXIS), RagDoll::RIGHT_CLAVICLE, 3, 1.5, -1, 1, "right upper arm"),
        new RagdollNode(PxQuat(PxPi - .3, Z_AXIS), RagDoll::LEFT_UPPER_ARM, 3, 1, -1, 1, "left lower arm"),
        new RagdollNode(PxQuat(0.3, Z_AXIS), RagDoll::RIGHT_UPPER_ARM, 3, 1, -1, 1, "right lower arm"),
        new RagdollNode(PxQuat(PxPi - .3, Z_AXIS), RagDoll::LEFT_LOWER_ARM, 0.5, 1, -0.75, 1, "left hand"),
        new RagdollNode(PxQuat(0.3, Z_AXIS), RagDoll::RIGHT_LOWER_ARM, 0.5, 1, -0.75, 1, "right hand"),
        NULL
    };

    m_renderer = new Renderer();
    m_physxScene.SetupPhysx();
    PxArticulation* ragDollArticulation;
    ragDollArticulation = RagDoll::MakeRagDoll(m_physxScene.g_Physics, ragdollData, PxTransform(PxVec3(0, 0, 0)), .1f, m_physxScene.g_PhysicsMaterial);
    m_physxScene.g_PhysicsScene->addArticulation(*ragDollArticulation);

    myHitReport = new MyControllerHitReport();
    gCharacterManager = PxCreateControllerManager(*m_physxScene.g_PhysicsScene);
    //describe our controller...
    PxCapsuleControllerDesc desc;
    desc.height = 1.6f;
    desc.radius = 0.4f;
    desc.position.set(0, 0, 0);
    desc.material = m_physxScene.g_PhysicsMaterial;
    desc.reportCallback = myHitReport;
    //connect it to our collision detection routine
    desc.density = 10; //create the layer controller
    gPlayerController = gCharacterManager->createController(desc);
    gPlayerController->setPosition(startingPosition);
    //set up some variables to control our player with
    _characterYVelocity = 0; //initialize character velocity
    _characterRotation = 0; //and rotation
    _playerGravity = -0.5f; //set up the player gravity
    myHitReport->clearPlayerContactNormal(); //initialize the contact normal (what we are in contact with)
    PhysxPhysics::setupFiltering(gPlayerController->getActor(), FilterGroup::ePLAYER, FilterGroup::eGROUND);
    gPlayerController->getActor()->setName("Player");
    m_physxScene.g_PhysicsScene->addActor(*gPlayerController->getActor()); //so we can draw it's gizmo

    m_physxScene.SetupVisualDebugger();
    m_physxScene.SetupObjects();

    return true;
}

void Physics::shutdown() {
    delete m_renderer;
    Gizmos::destroy();
    Application::shutdown();
    m_physxScene.CleanPhysx();
}

bool Physics::update() {
    if (Application::update() == false) {
        return false;
    }

    Gizmos::clear();

    float dt = (float)glfwGetTime();
    m_delta_time = dt;
    glfwSetTime(0.0);

    //vec4 white(1);
    //vec4 black(0, 0, 0, 1);

    //for (int i = 0; i <= 20; ++i) {
    //    Gizmos::addLine(vec3(-10 + i, -0.01, -10), vec3(-10 + i, -0.01, 10),
    //                    i == 10 ? white : black);
    //    Gizmos::addLine(vec3(-10, -0.01, -10 + i), vec3(10, -0.01, -10 + i),
    //                    i == 10 ? white : black);
    //}

    GLFWwindow* curr_window = glfwGetCurrentContext();

    bool onGround;
    //set to true if we are on the ground 
    float movementSpeed = 10.0f;
    //forward and back movement speed
    float rotationSpeed = 1.0f;
    //turn speed
    //check if we have a contact normal. if y is greater than .3 we assume this is solid ground. This is a rather primitive way to do this. Can you do better?
    if (myHitReport->getPlayerContactNormal().y > 0.3f) {
        _characterYVelocity = -0.1f;
        onGround = true;
    }
    else {
        _characterYVelocity += _playerGravity * dt;
        onGround = false;
    }
    myHitReport->clearPlayerContactNormal();
    const PxVec3 up(0, 1, 0); //scan the keys and set up our intended velocity based on a global transform
    PxVec3 velocity(0, _characterYVelocity, 0);
    if (glfwGetKey(curr_window, GLFW_KEY_UP) == GLFW_PRESS) {
        velocity.x -= movementSpeed * dt;
    }
    if (glfwGetKey(curr_window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        velocity.x += movementSpeed * dt;
    }
    if (glfwGetKey(curr_window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        velocity.z -= movementSpeed * dt;
    }
    if (glfwGetKey(curr_window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        velocity.z += movementSpeed * dt;
    }
    //To do.. add code to control z movement and jumping
    float minDistance = 0.001f;
    PxControllerFilters filter;
    //make controls relative to player facing
    PxQuat rotation(_characterRotation, PxVec3(0, 1, 0));
    //move the controller
    gPlayerController->move(rotation.rotate(velocity), minDistance, dt, filter);

    m_camera.update(1.0f / 60.0f);
    m_physxScene.UpdatePhysx(dt);
    
    if (glfwGetMouseButton(curr_window, 0) == GLFW_PRESS) {
        m_physxScene.ShootBall(m_camera);
    }
    renderGizmos(m_physxScene.g_PhysicsScene);
    return true;
}

void Physics::draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_CULL_FACE);
    Gizmos::draw(m_camera.proj, m_camera.view);

    m_renderer->RenderAndClear(m_camera.view_proj);

    glfwSwapBuffers(m_window);
    glfwPollEvents();
}

void AddWidget(PxShape* shape, PxRigidActor* actor, vec4 geo_color) {
    PxTransform full_transform = PxShapeExt::getGlobalPose(*shape, *actor);
    vec3 actor_position(full_transform.p.x, full_transform.p.y, full_transform.p.z);
    glm::quat actor_rotation(full_transform.q.w,
                             full_transform.q.x,
                             full_transform.q.y,
                             full_transform.q.z);
    glm::mat4 rot(actor_rotation);

    mat4 rotate_matrix = glm::rotate(10.f, glm::vec3(7, 7, 7));

    PxGeometryType::Enum geo_type = shape->getGeometryType();

    switch (geo_type) {
        case (PxGeometryType::eBOX): {
            PxBoxGeometry geo;
            shape->getBoxGeometry(geo);
            vec3 extents(geo.halfExtents.x, geo.halfExtents.y, geo.halfExtents.z);
            Gizmos::addAABBFilled(actor_position, extents, geo_color, &rot);
        }
            break;
        case (PxGeometryType::eCAPSULE): {
            PxCapsuleGeometry geo;
            shape->getCapsuleGeometry(geo);
            Gizmos::addCapsule(actor_position, geo.halfHeight * 2, geo.radius, 16, 16, geo_color, &rot);
        }
            break;
        case (PxGeometryType::eSPHERE): {
            PxSphereGeometry geo;
            shape->getSphereGeometry(geo);
            Gizmos::addSphereFilled(actor_position, geo.radius, 16, 16, geo_color, &rot);
        }
            break;
        case (PxGeometryType::ePLANE): { }
            break;
    }
}

void Physics::renderGizmos(PxScene* physics_scene) {
    PxActorTypeFlags desiredTypes = PxActorTypeFlag::eRIGID_STATIC | PxActorTypeFlag::eRIGID_DYNAMIC;
    PxU32 actor_count = physics_scene->getNbActors(desiredTypes);
    PxActor** actor_list = new PxActor*[actor_count];
    physics_scene->getActors(desiredTypes, actor_list, actor_count);

    vec4 geo_color(1, 0, 0, 1);
    for (int actor_index = 0;
         actor_index < (int)actor_count;
         ++actor_index) {
        PxActor* curr_actor = actor_list[actor_index];
        if (curr_actor->isRigidActor()) {
            PxRigidActor* rigid_actor = (PxRigidActor*)curr_actor;
            PxU32 shape_count = rigid_actor->getNbShapes();
            PxShape** shapes = new PxShape*[shape_count];
            rigid_actor->getShapes(shapes, shape_count);

            for (int shape_index = 0;
                 shape_index < (int)shape_count;
                 ++shape_index) {
                PxShape* curr_shape = shapes[shape_index];
                AddWidget(curr_shape, rigid_actor, geo_color);
            }

            delete[]shapes;
        }
    }

    delete[] actor_list;

    int articulation_count = physics_scene->getNbArticulations();

    for (int a = 0; a < articulation_count; ++a) {
        PxArticulation* articulation;
        physics_scene->getArticulations(&articulation, 1, a);

        int link_count = articulation->getNbLinks();

        PxArticulationLink** links = new PxArticulationLink*[link_count];
        articulation->getLinks(links, link_count);

        for (int l = 0; l < link_count; ++l) {
            PxArticulationLink* link = links[l];
            int shape_count = link->getNbShapes();

            for (int s = 0; s < shape_count; ++s) {
                PxShape* shape;
                link->getShapes(&shape, 1, s);
                AddWidget(shape, link, geo_color);
            }
        }
        delete[] links;
    }
}

