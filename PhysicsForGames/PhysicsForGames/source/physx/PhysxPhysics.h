#pragma once
#include <PxPhysicsAPI.h>
#include <PxScene.h>
#include <pvd/PxVisualDebugger.h>
#include "../Camera.h"
#include "../../ParticleEmitter.h"
#include "../../ParticleFluidEmitter.h"

using namespace physx;

class PhysxPhysics {
public:
    PhysxPhysics();
    ~PhysxPhysics();
    void SetupPhysx();
    void SetupVisualDebugger();
    void SetupObjects();
    void SetupFluidObjects();
    void ShootBall(FlyCamera camera);
    void UpdatePhysx(float deltaTime) const;
    void CleanPhysx() const;
    static void setupFiltering(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask);
    void setShapeAsTrigger(PxRigidStatic* actorIn);

    PxFoundation* g_PhysicsFoundation;
    PxPhysics* g_Physics;
    PxScene* g_PhysicsScene;
    PxDefaultErrorCallback gDefaultErrorCallback;
    PxDefaultAllocator gDefaultAllocatorCallback;
    PxSimulationFilterShader gDefaultFilterShader = PxDefaultSimulationFilterShader;
    PxMaterial* g_PhysicsMaterial;
    PxMaterial* g_boxMaterial;
    PxCooking* g_PhysicsCooker;
    ParticleFluidEmitter* m_particleEmitter;
};

struct FilterGroup {
    enum Enum {
        ePLAYER = (1 << 0),
        ePLATFORM = (1 << 1),
        eGROUND = (1 << 2)
    };
};

