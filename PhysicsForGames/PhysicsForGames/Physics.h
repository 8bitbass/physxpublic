#ifndef SOUND_PROGRAMMING_H_
#define SOUND_PROGRAMMING_H_

#include "Application.h"
#include "Camera.h"
#include "Render.h"

#include <PxPhysicsAPI.h>
#include <PxScene.h>
#include "source/physx/PhysxPhysics.h"
#include "source/physx/MyControllerHitReport.h"

using namespace physx;
class Physics : public Application
{
public:
    virtual bool startup();
    virtual void shutdown();
    virtual bool update();
    virtual void draw();

    void renderGizmos(PxScene* physics_scene);

    Renderer* m_renderer;
    FlyCamera m_camera;
    float m_delta_time;
    PhysxPhysics m_physxScene;
    MyControllerHitReport* myHitReport;
    PxControllerManager* gCharacterManager;
    PxController* gPlayerController;
    float _characterYVelocity;
    float _characterRotation;
    float _playerGravity;
    PxExtendedVec3 startingPosition = PxExtendedVec3(0,2,0);
};



#endif //CAM_PROJ_H_
